Module table
================================================================================

DESCRIPTION:
--------------------------------------------------------------------------------
This module is intended to replace the regular module administration page with 
a more intuitive, task-oriented user interface for administer modules.

INSTALLATION:
--------------------------------------------------------------------------------
1. Download and the required jquery_plugin module 
     (http://drupal.org/project/jquery_plugin).

2. Install the jquery_plugin module by navigating to:
     admin/build/modules

3. Download and the required float_window module 
     (http://drupal.org/project/float_window).

4. Install the float_window module by navigating to:
     admin/build/modules

5. Download the jQuery plugin tablesorter and move jquery.tablesorter.min.js to 
   the jquery_plugin module folder.
     (http://plugins.jquery.com/project/tablesorter)

6. Download the jQuery plugin jquery-tooltip and move jquery.tooltip.min.js to
   the jquery_plugin module folder.
     (http://bassistance.de/jquery-plugins/jquery-plugin-tooltip/)

7. Download the jQuery plugin Dimensions and move jquery.dimensions.min.js to
   the jquery_plugin module folder.
     (http://plugins.jquery.com/project/dimensions)

8. Download the jQuery plugin checkboxToSlider and save it as 
   jquery.checkboxToSlider.min.js inside the jquery_plugin module folder.
     (http://plugins.jquery.com/project/checkboxToSlider)

9. Install the module_table module by navigating to:
     admin/build/modules

10. See the module in action here:
     admin/build/modules/table
