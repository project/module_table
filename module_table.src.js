function moduleTable() {
  var mt                  = this;
  mt.sort_list            = (Drupal.settings.module_table.user_tablesort) || [2,0];
  mt.table_obj            = $("#modules_table");
  mt.filter_obj           = $("#edit-filter");
  mt.ajax_response_path   = Drupal.settings.module_table.ajax_response_path;
  mt.modules              = Drupal.settings.module_table.modules;
  mt.module_path          = Drupal.settings.basePath + Drupal.settings.module_table.module_table_path;
  mt.ajax_complete        = true;

  /**
   * Initializes the moduleTable
   */
  mt.init = function() {
    mt.addTooltip();
    mt.configurationsAddTooltip();
    mt.addtableSorter();
    mt.addSliders();
    mt.permissionsAddEventListeners();
    mt.addtableFilter();
  };
  
  /**
   * 
   */
  mt.permissionsAddEventListeners = function() {
    mt.table_obj.find('.column_permissions a').mousedown(function() {
      window.float_windows.permissions.content_properties.ajax_params.module = $(this).attr('name');
      return false;
    });
  };
    
  /**
   * Adds tooltips to the configuration icons
   */
  mt.configurationsAddTooltip = function(tr_obj) {
    var table, admin_tasks;
    var target = tr_obj || mt.table_obj;

    target.find('.module_table_config_wrapper').hover(function() {
      $(this).css({'width':'auto'});
      admin_tasks = $(this).find('.admin_tasks');
      table = Drupal.tooltip_table.clone().appendTo($(this));
      table.width(admin_tasks.width() + 10).find('td.middle_center').append(admin_tasks.html());
    }, function() {
      $(this).css({'width':'27px'});
      table.remove();
    }).children('a').click(function() { return false;});
  };
    
  /**
   * Adds tooltipt to the info icons
   * Uses the tooltip plugin
   */
  mt.addTooltip = function() {
    // Create a table for the tooltip  
    var hor_classes   = ['left', 'center', 'right'];
    var vert_classes  = ['top', 'middle', 'bottom'];
    var trs = '';
    
    for(var vk in vert_classes) {
      trs += '<tr>';
      for(var hk in hor_classes) trs += '<td class="' + vert_classes[vk] + '_' + hor_classes[hk] + '"></td>';
      trs += '</tr>';
    };
    
    // Add it to the Drupal object as reference 
    Drupal.tooltip_table = $('<table id="module_description_table">' + trs + '</table>');
    
    // Add the tooltip to each info image.
    mt.table_obj.find('.column_info img').tooltip({
      bodyHandler: function() {
        var text = $(this).parent().parent().find('.module_description').html();
        var table = Drupal.tooltip_table.clone();
        table.find('td.middle_center').html(text);
        return table;
      },
      track:true,
      top:-10,
      left:10,
      delay:0,
      showURL:false
    });
  };
  
  /**
   * Add filter functionality to the module tabe
   */
  mt.addtableFilter = function() {
    var filter_value, str_exists;

    mt.filter_obj.keyup(function() {
      filter_value = $(this).val().toLowerCase();
      mt.table_obj.find('tbody tr').each(function() {
        str_exists = false;
        $(this).find('td.column_name, td.column_version, td.column_package').each(function() {
          if ($(this).text().toLowerCase().indexOf(filter_value) > -1) {
            str_exists = true;
          };
        });
        if (!str_exists) {
          $(this).hide();
        }
        else {
          $(this).show();
        };
      });
    });
  };
  
  /**
   * Add sortable functionality to the module tabe
   * Uses the tablesorter plugin
   */
  mt.addtableSorter = function() {
    // add parser through the tablesorter addParser method 
    // This parser will check the status of a "module enabled" checkbox inside the Drupal.settings.module_table.modules object.
    $.tablesorter.addParser({
      // set a unique id 
      id: 'status',
      is: function(s) {
        // return false so this parser is not auto detected 
        return false; 
      },
      format: function(s) {
        // format your data for normalization 
        var name_start  = s.indexOf('name="') + 6;
        var name_end    = s.indexOf('"', name_start);
        var name = s.substr(name_start, (name_end - name_start));
        return (Drupal.settings.module_table.modules[name]) ? 1 : 0;
      },
      // set type, either numeric or text 
      type: 'numeric'
    }); 

    mt.table_obj.tablesorter({
      sortList: [mt.sort_list],
      headers: {5:{sorter:'status'}},
      widgets: ["zebra"]
    }).bind('sortEnd', function() {
      mt.saveSortStatus();
    });
  };

  /**
   * Replaces the checkboxes in the 'enabled' column of the module table with sliders
   * Uses the checkboxToSlider plugin
   */
  mt.addSliders = function() {
    mt.table_obj.find('input:checkbox').each(function() {
      $(this).checkboxToSlider({
        slider_width:39,
        container_width:70,
        disable_styles:true,
        start:function() {return mt.ajax_complete},
        change:function(status, checkbox_obj) {
          Drupal.settings.module_table.modules[checkbox_obj.attr('name')] = status;
          mt.saveModuleStatus(checkbox_obj.attr('name'), checkbox_obj.parents('tr').find('td:eq(0)').text(), status);
          mt.table_obj.trigger("update");
        }
      }).append($('<div />').addClass('slider_mask_left')).append($('<div />').addClass('slider_mask_right'));
    });
  };
  
  /**
   * This function saves the sort status after a user has sorted the module table
   */
  mt.saveSortStatus = function() {
    mt.ajax_complete = false;
    var col_counter = 0;
    
    mt.table_obj.find('th').each(function() {
      var curr_class = $(this).attr('class');
      if(curr_class && ((curr_class.indexOf('headerSortDown') > -1) || (curr_class.indexOf('headerSortUp') > -1))) {
        $.ajax({
          url:mt.ajax_response_path,
          type:'post',
          dataType:'json',
          complete:function() {
            mt.ajax_complete = true;
          },
          data:{
            sort_column:col_counter,
            sort_direction:(curr_class.indexOf('Down') > -1) ? 0 : 1
          }
        });
      };
      col_counter++;
    });
  };
  
  /**
   * Saves the enabled/disabled status of a module enabling/disabling 
   */
  mt.saveModuleStatus = function(module, name, status) {
    mt.ajax_complete = false;
    mt.displayStatus(module, 'loading');

    $.ajax({
      url:mt.ajax_response_path,
      type:'post',
      dataType:'json',
      success:function(json) {
        mt.handleAJAXResponse(json);
      },
      data:{
        status: (status) ? 1 : 0,
        name:   name,
        module: module
      }
    });
  };
  
  /**
   * Displayes the status of the AJAX request
   */
  mt.displayStatus = function(module, status, animate) {
    var tr_obj = $('#tr-' + module);
    var status_obj = tr_obj.find('td.column_status div').css('opacity', 0).attr('class', status);

    if (animate) {
      status_obj.animate({opacity:1}, 'slow', function() {
        delete(mt.status_timeout);
        status_obj.status_timeout = setTimeout(function() {
          status_obj.animate({opacity:0}, 'fast');
        }, 4000);
      });
    }
    else {
      status_obj.css('opacity', 1);
    };

    if (status == 'uninstall') {
      status_obj.css({'cursor':'pointer'}).click(function() {
        mt.confirmUninstall();
      });
    };
  };
  
  /**
   * 
   */
  mt.updateModuleTr = function(json) {
    var tr_obj = $('#tr-' + json.module).removeClass('enabled disabled').addClass(json.action + 'd');

    // Reset the administration tasks icon.
    tr_obj.find('td.column_configurations').html(json.admin_tasks_icon);
    if (json.admin_tasks_icon !== '') {
      mt.configurationsAddTooltip(tr_obj);
    };

    // Reset the permissions icon(s).
    // If another user has disabled the permission to administer permissions for the current user, hide all perm icons.
    $('tr.enabled td.column_permissions a.has_perms').css({'visibility':(json.administer_permissions ? 'visible' : 'hidden')});
    tr_obj.find('td.column_permissions a').css({'visibility':(json.action == 'enable' && json.permission_icon !== '' ? 'visible' : 'hidden')});

    // Notify the user of the success
    mt.displayStatus(json.module, (json.uninstall ? 'uninstall' : 'success'), (!json.uninstall));
  };
  
  /**
   * Handles AJAX responses
   * Places or removes administration tasks and permission icons
   * Shows the response messages and redirects the browser if needed
   */
  mt.handleAJAXResponse = function(json) {
    mt.updateModuleTr(json);

    if(Drupal.inline_dialog) {
      if(json.redirect) {
        if(json.messages) {
          Drupal.inline_dialog.redirect = json.redirect;
        } else {
          window.location.href = json.redirect;
        }
      }
      if(json.messages) {
        Drupal.settings.messages = json.messages;
        Drupal.inline_dialog.drupalMessages();
      };
    } else {
      if(json.messages) {
        for (var i in json.messages) {
          alert(json.messages[i]);
        };
      };
      if(json.redirect) window.location.href = json.redirect;
    };
    mt.ajax_complete = true;
  };

  /**
   * 
   */
  mt.confirmUninstall = function() {
    if (confirm('unisntall?')) {
      mt.moduleUninstall();
    };
  };

  /**
   * 
   */
  mt.moduleUninstall = function() {
    alert('unisntall!');
  };
};

function permissionsSuccessCallbackFunction() {
  alert('success');
}

$(function() {
  Drupal.moduleTable = new moduleTable();
  Drupal.moduleTable.init();
});